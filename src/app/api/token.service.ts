import { Injectable } from '@angular/core';
import {
  OAuthProviderClientCredentials,
  REDIRECT_PREFIX,
} from '../../environments/environment';
import {
  STATE,
  EXPIRES_IN,
  ACCESS_TOKEN,
  REVOCATION_URL,
  TOKEN_URL,
  CLIENT_ID,
  REFRESH_TOKEN,
  CODE,
  LOGGED_IN,
  ONE_HOUR_IN_SECONDS_STRING,
  ID_TOKEN,
} from '../constants/storage';
import { of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { stringify } from 'querystring';
import { switchMap, catchError } from 'rxjs/operators';
import { parse } from 'url';
import {
  InAppBrowser,
  InAppBrowserObject,
} from '@ionic-native/in-app-browser/ngx';
import { StorageService } from './storage.service';

export const STATE_LENGTH = 32;

@Injectable({
  providedIn: 'root',
})
export class TokenService {
  state: string;
  authWindow: InAppBrowserObject;

  constructor(
    private iab: InAppBrowser,
    private http: HttpClient,
    private storage: StorageService,
  ) {}

  initializeCodeGrant() {
    this.state = this.generateState();
    this.storage.store(STATE, this.state);
    let authRequestUrl = OAuthProviderClientCredentials.authorizationUrl;
    authRequestUrl += '?scope=' + OAuthProviderClientCredentials.scope;
    authRequestUrl += '&response_type=code&client_id=';
    authRequestUrl += OAuthProviderClientCredentials.clientId;
    authRequestUrl += '&redirect_uri=';
    authRequestUrl += OAuthProviderClientCredentials.appUrl + REDIRECT_PREFIX;
    authRequestUrl += '&state=' + this.state;

    this.authWindow = this.iab.create(authRequestUrl, '_blank', {
      location: 'no',
      zoom: 'no',
      clearcache: 'yes',
    });

    this.authWindow.on('loadstart').subscribe({
      next: event => {
        if(event.url.startsWith(OAuthProviderClientCredentials.appUrl)) {
          this.onOauth2Callback(event.url);
        }
      },
      error: error => {},
    });

    this.authWindow.on('exit').subscribe({
      next: event => this.authWindow.close(),
      error: error => this.authWindow.close(),
    });
  }

  generateState() {
    let result = '';
    const characters =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < STATE_LENGTH; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  getToken() {
    const expiration = localStorage.getItem(EXPIRES_IN);
    if (expiration) {
      const now = new Date();
      const expirationTime = new Date(expiration);

      // expire 10 min early
      expirationTime.setSeconds(
        expirationTime.getSeconds() - 600,
      );

      if (now < expirationTime) {
        const accessToken = localStorage.getItem(ACCESS_TOKEN);
        return of(accessToken);
      }
      return this.refreshToken().pipe(
        catchError(error => {
          this.storage.clear(ACCESS_TOKEN);
          this.storage.clear(REFRESH_TOKEN);
          this.storage.clear(LOGGED_IN);
          return of();
        }),
      );
    }
    return of();
  }

  refreshToken() {
    const tokenURL = localStorage.getItem(TOKEN_URL);
    const requestBody = {
      grant_type: 'refresh_token',
      refresh_token: localStorage.getItem(REFRESH_TOKEN),
      redirect_uri: OAuthProviderClientCredentials.appUrl + REDIRECT_PREFIX,
      client_id: localStorage.getItem(CLIENT_ID),
      // scope: localStorage.getItem(SCOPE),
    };

    return this.http
      .post<any>(tokenURL, stringify(requestBody), {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      })
      .pipe(
        switchMap(bearerToken => {
          this.revokeToken(bearerToken.access_token, bearerToken.refresh_token);
          const expirationTime = new Date();
          const expiresIn =
            bearerToken.expires_in || ONE_HOUR_IN_SECONDS_STRING;
          expirationTime.setSeconds(
            expirationTime.getSeconds() + Number(expiresIn),
          );
          this.storage.store(EXPIRES_IN, expirationTime.toISOString());
          this.storage.store(ID_TOKEN, bearerToken.id_token);
          return of(bearerToken.access_token);
        }),
      );
  }

  revokeToken(accessToken?: string, refreshToken?: string) {
    const revocationURL = localStorage.getItem(REVOCATION_URL);
    const oldAccessToken = localStorage.getItem(ACCESS_TOKEN);
    this.http.get(revocationURL + '?token=' + oldAccessToken).subscribe({
      next: success => {
        if (accessToken) {
          this.storage.store(ACCESS_TOKEN, accessToken);
        }
        if (refreshToken) {
          this.storage.store(REFRESH_TOKEN, refreshToken);
        }
      },
      error: error => {},
    });
  }

  onOauth2Callback(callbackUrl) {
    const urlParts = parse(callbackUrl, true);
    const query = urlParts.query;
    const code = query.code as string;
    const state = query.state as string;
    const error = query.error;

    if (this.state !== state) {
      return;
    }
    if (error !== undefined) {
      return;
    } else if (code) {
      this.storage.store(CODE, code);
    }
    this.authWindow.close();
  }
}
