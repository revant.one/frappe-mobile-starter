import { Injectable } from '@angular/core';
import {
  OAuthProviderClientCredentials,
  REDIRECT_PREFIX,
} from '../environments/environment';
import { StorageService } from './api/storage.service';
import {
  CODE,
  LOGGED_IN,
  ACCESS_TOKEN,
  REFRESH_TOKEN,
  STATE,
  CLIENT_ID,
  SCOPE,
  TOKEN_URL,
  EXPIRES_IN,
  ID_TOKEN,
  ONE_HOUR_IN_SECONDS_STRING,
} from './constants/storage';
import { stringify } from 'querystring';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class AppService {
  constructor(private storage: StorageService, private http: HttpClient) {
    this.storage.changes.subscribe({
      next: event => {
        if (event.key === CODE && event.value) {
          const state = localStorage.getItem(STATE);
          this.storage.clear(CODE);
          this.storage.clear(STATE);
          this.processCode(event.value, state);
        }
      },
      error: error => {},
    });
  }

  setupOauthConfig() {
    for (const key of Object.keys(OAuthProviderClientCredentials)) {
      this.storage.store(key, OAuthProviderClientCredentials[key]);
    }
  }

  watchStore() {
    return this.storage.changes;
  }

  processCode(code: string, state: string) {
    const req: any = {
      grant_type: 'authorization_code',
      code,
      redirect_uri: OAuthProviderClientCredentials.appUrl + REDIRECT_PREFIX,
      client_id: localStorage.getItem(CLIENT_ID),
      scope: localStorage.getItem(SCOPE),
    };
    const url = localStorage.getItem(TOKEN_URL);
    this.http
      .post<any>(url, stringify(req), {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      })
      .subscribe({
        next: response => {
          const expiresIn = response.expires_in || ONE_HOUR_IN_SECONDS_STRING;
          const expirationTime = new Date();
          expirationTime.setSeconds(
            expirationTime.getSeconds() + Number(expiresIn),
          );

          this.storage.store(ACCESS_TOKEN, response.access_token);
          this.storage.store(REFRESH_TOKEN, response.refresh_token);
          this.storage.store(EXPIRES_IN, expirationTime.toISOString());
          this.storage.store(ID_TOKEN, response.id_token);
          this.storage.store(LOGGED_IN, 'true');
        },
        error: error => {
          this.storage.clear(LOGGED_IN);
        },
      });
  }

  clear() {
    this.storage.clear(LOGGED_IN);
    this.storage.clear(ACCESS_TOKEN);
    this.storage.clear(REFRESH_TOKEN);
    this.storage.clear(EXPIRES_IN);
  }
}
