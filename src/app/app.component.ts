import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppService } from './app.service';
import { Router } from '@angular/router';
import { TokenService } from './api/token.service';
import { LOGGED_IN } from './constants/storage';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
})
export class AppComponent implements OnInit {
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home',
    },
    {
      title: 'List',
      url: '/list',
      icon: 'list',
    },
  ];
  loggedIn: boolean;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private readonly appService: AppService,
    private readonly token: TokenService,
    private readonly router: Router,
  ) {
    this.initializeApp();
  }

  ngOnInit() {
    this.appService.watchStore().subscribe({
      next: event => {
        if (event.key === LOGGED_IN) {
          this.loggedIn = event.value ? true : false;
        }
      },
      error: error => {},
    });
    this.loggedIn = localStorage.getItem(LOGGED_IN) ? true : false;
  }

  login() {
    this.token.initializeCodeGrant();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.appService.setupOauthConfig();
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  logout() {
    this.token.revokeToken();
    this.appService.clear();
    this.router.navigateByUrl('/home');
  }
}
