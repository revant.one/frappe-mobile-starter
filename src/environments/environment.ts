// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
export const OAuthProviderClientCredentials = {
  profileUrl:
    'https://default.mntechnique.com/api/method/frappe.integrations.oauth2.openid_profile',

  tokenUrl:
    'https://default.mntechnique.com/api/method/frappe.integrations.oauth2.get_token',

  authServerUrl: 'https://default.mntechnique.com',

  authorizationUrl:
    'https://default.mntechnique.com/api/method/frappe.integrations.oauth2.authorize',

  revocationUrl:
    'https://default.mntechnique.com/api/method/frappe.integrations.oauth2.revoke_token',

  scope: 'all%20openid',

  clientId: '83bdec290f',

  appUrl: 'http://localhost:8100',
};

export const REDIRECT_PREFIX = '/callback';
