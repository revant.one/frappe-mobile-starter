### Ionic Starter for Frappe

- Uses frappe's OAuth2 for authorization
- Seamless session refresh with refresh token (for offline abilities)
- Minimum Unit tests and E2E tests to ensure API functionality and upgrades

### Bootstrap development

#### Prerequisites
- nvm (recommended)
- ionic `npm i -g ionic`

```sh
npm i

# development
ionic cordova run android --device -l --debug
```

Note:
- To avoid Frappe CORS Issue, the HTTP requests are made using native calls.
- App does not work in browser

### VS Code Configuration

`launch.json` file, replace as per your operating system:

```json
{
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
        {
            "type": "chrome",
            "request": "launch",
            "name": "ionic serve",
            "url": "http://localhost:8100",
            "runtimeExecutable": "/usr/bin/chromium",
            "runtimeArgs": [
                "--disable-web-security",
                "--remote-debugging-port=9222",
                "--user-data-dir=/tmp/nocors"
            ],
            "webRoot": "${workspaceFolder}"
        }
    ]
}
```
